{
  description = "Home Manager configuration of David Hintringer";

  inputs = {
    # Specify the source of Home Manager and Nixpkgs.
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";
    # Additionally, make the unstable branch available
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };

    # Emacs overlay
    emacs-overlay.url = "github:nix-community/emacs-overlay";
    # Doom Emacs
    nix-doom-emacs.url = "github:nix-community/nix-doom-emacs";

    # Hyprpicker
    hyprpicker.url = "github:hyprwm/hyprpicker";
  };

  outputs = { nixpkgs, nixpkgs-unstable, home-manager, nix-doom-emacs, hyprpicker, emacs-overlay, ... }:
    let
      # Utilities
      recursiveMerge = import ./utils/recursiveMerge.nix { lib = nixpkgs.lib; };
      # Constants
      system = "x86_64-linux";


      # Home manager definitions
      # --------------------------
      pkgs = nixpkgs.legacyPackages.${system};
      pkgs-unstable = nixpkgs-unstable.legacyPackages.${system}.appendOverlays [ emacs-overlay.overlays.emacs ] // hyprpicker.packages.${system};
      username = "david";
      homeDirectory = /home/david;
      # Home manager module
      home = import ./home.nix {inherit pkgs; inherit pkgs-unstable; inherit username; inherit home-manager; inherit homeDirectory; inherit recursiveMerge; inherit nix-doom-emacs; };
    in {
      	homeConfigurations.${username} = home;
      };
}
