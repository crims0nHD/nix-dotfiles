{pkgs, pkgs-unstable, username, home-manager, homeDirectory, recursiveMerge, nix-doom-emacs, ...}:
home-manager.lib.homeManagerConfiguration {
inherit pkgs;
# Specify your home configuration modules here, for example,
# the path to your home.nix.
modules = [
	nix-doom-emacs.hmModule
	./home
	];

# Optionally use extraSpecialArgs
# to pass through arguments to home.nix
extraSpecialArgs = {
	inherit pkgs-unstable;
	inherit username;
	inherit homeDirectory;
	inherit recursiveMerge;
};

}
