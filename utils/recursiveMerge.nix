# I'm amazed by the sheer dumbness of nix

# Todo: fucking understand it

{lib, ...}:
with lib;
let
recursiveMerge = attributeList:
	let f = attributePath:
		builtins.zipAttrsWith (n: values:
			if tail values == []
				then head values
			else if all isList values
				then unique (concatLists values)
			else if all isAttrs values
				then f (attributePath ++ [n]) values
			else last values
			);
	in f [] attributeList;

in
recursiveMerge
