{ pkgs, pkgs-unstable, config, username, homeDirectory, ... }:
{
  home = {
    inherit username;
    inherit homeDirectory;

    stateVersion = "22.11";
    packages = with pkgs; [
    	# Desktop apps	
	pavucontrol

	# Terminal stuff
	neovim
	git
	htop
	neofetch
	
	# Wayland utilities
	wl-clipboard
	pkgs-unstable.hyprpicker

	# CPP development stuff
	clang
	clang-tools
	lldb
	gnumake
	cmake

	# Dependencies
	python311
	fira-code
	emacs-all-the-icons-fonts
    ];

  };
  
  # NOTE: DO NOT REMOVE THIS LINE!!! OTHERWISE U HAVE TO MANUALLY CALL HOME-MANAGER
  programs.home-manager.enable = true;

}
