{pkgs, pkgs-unstable, ...}:
{
  # Doom emacs
  programs.doom-emacs = {
	enable = true;
	doomPrivateDir = pkgs.callPackage ./doom.d {};
	emacsPackage = pkgs-unstable.emacsPgtk;
  };
	
}
