{ version ? "dev", lib, stdenv, emacs }:

stdenv.mkDerivation {
  pname = "doom-emacs-config";
  inherit version;
  src = ./.;

  buildInputs = [emacs];

  buildPhase = ''
    cp -r $src/* .
    # Tangle org files
    emacs --batch -Q \
      -l org \
      config.org \
      -f org-babel-tangle
  '';

  dontUnpack = true;

  installPhase = ''
    mkdir -p $out
    cp -r * $out
    chmod a+r -R *
  '';
}
