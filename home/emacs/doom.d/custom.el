(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#000000" "#ff6c6b" "#98be65" "#ECBE7B" "#0170bf" "#c678dd" "#46D9FF" "#bbc2cf"])
 '(auth-source-save-behavior nil)
 '(custom-safe-themes
   '("cbdf8c2e1b2b5c15b34ddb5063f1b21514c7169ff20e081d39cf57ffee89bc1e" "da53441eb1a2a6c50217ee685a850c259e9974a8fa60e899d393040b4b8cc922" "8d7b028e7b7843ae00498f68fad28f3c6258eda0650fe7e17bfb017d51d0e2a2" "7a7b1d475b42c1a0b61f3b1d1225dd249ffa1abb1b7f726aec59ac7ca3bf4dae" "8fd2d543bdaa2858240dd7814bd8ee78c869f90bffed37a06316dd6407510260" default))
 '(exwm-floating-border-color "#1c1f24")
 '(fci-rule-color "#5B6268")
 '(highlight-tail-colors ((("#0f130a") . 0) (("#071519") . 20)))
 '(jdee-db-active-breakpoint-face-colors (cons "#1B2229" "#0170bf"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1B2229" "#98be65"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1B2229" "#5B6268"))
 '(objed-cursor-color "#ff6c6b")
 '(package-selected-packages '(w3m counsel gitconfig epc))
 '(pdf-view-midnight-colors (cons "#bbc2cf" "#000000"))
 '(rustic-ansi-faces
   ["#000000" "#ff6c6b" "#98be65" "#ECBE7B" "#0170bf" "#c678dd" "#46D9FF" "#bbc2cf"])
 '(vc-annotate-background "#000000")
 '(vc-annotate-color-map
   (list
    (cons 20 "#98be65")
    (cons 40 "#b4be6c")
    (cons 60 "#d0be73")
    (cons 80 "#ECBE7B")
    (cons 100 "#d9af76")
    (cons 120 "#c6a071")
    (cons 140 "#b4916d")
    (cons 160 "#ba8892")
    (cons 180 "#c080b7")
    (cons 200 "#c678dd")
    (cons 220 "#d974b7")
    (cons 240 "#ec7091")
    (cons 260 "#ff6c6b")
    (cons 280 "#d6696a")
    (cons 300 "#ad6769")
    (cons 320 "#836468")
    (cons 340 "#5B6268")
    (cons 360 "#5B6268")))
 '(vc-annotate-very-old-color nil)
 '(warning-suppress-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
