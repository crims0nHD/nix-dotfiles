{ pkgs, pkgs-unstable, config, username, homeDirectory, recursiveMerge, ... }:
let
	home-legacy = import ./home-legacy.nix { inherit pkgs; inherit pkgs-unstable; inherit config; inherit username; inherit homeDirectory;};
	gui = import ./gui {inherit pkgs; inherit pkgs-unstable; inherit recursiveMerge;};
	nix-config = import ./nix-config.nix {inherit pkgs;};
	emacs = import ./emacs {inherit pkgs; inherit pkgs-unstable; };
	cli = import ./cli {};
	
	merge = recursiveMerge [home-legacy gui nix-config emacs cli];
in
{
	home = merge.home;
	programs = merge.programs;
	nix = merge.nix;
	fonts = merge.fonts;
}
