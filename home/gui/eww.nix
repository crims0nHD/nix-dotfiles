# Eww nix configuration for home-manager

{pkgs, pkgs-unstable, ...}:
{
	home= {
		file.".config/eww" = {
			source = ./eww;
			recursive = true;
		};

	packages = [
		pkgs-unstable.eww-wayland
	];
	};
}
