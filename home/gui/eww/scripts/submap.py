#!/usr/bin/env python3

import subprocess
import os
import socket

submap_keyword = "submap>>"

def isKeyword(keyword, msg):
    return msg[0:len(keyword)] == keyword

def update_submap(name):
    if name == "":
        name = "normal"

    prompt = f"(box (label :text \"{name}\" ))"
    
    subprocess.run(f"echo '{prompt}'", shell=True)

# Main script starting point
if __name__=="__main__":
    # Connect to hyprland socket
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    server_address = f'/tmp/hypr/{os.environ["HYPRLAND_INSTANCE_SIGNATURE"]}/.socket2.sock'
    sock.connect(server_address)

    # Main event loop
    while True:
        new_event = sock.recv(4096).decode("utf-8")
        
        for item in new_event.split("\n"):
            if isKeyword(submap_keyword, item):
                submap_name = item[len(submap_keyword)-1:-1]
                update_submap(submap_name)
