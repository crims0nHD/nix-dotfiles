#!/usr/bin/env python3

import subprocess
import os
import socket

def update_workspace(active_workspace):
    workspaces = ""
    for i in range(1,4):
        if active_workspace == i:
            workspaces = workspaces + f"[{i}] "
        else:
            workspaces = workspaces + f"{i} "
    workspaces = workspaces[:len(workspaces)-1]

    prompt = f"(box (label :text \"{workspaces}\" ))"
    
    subprocess.run(f"echo '{prompt}'", shell=True)

# Main script starting point
if __name__=="__main__":
    # Connect to hyprland socket
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    server_address = f'/tmp/hypr/{os.environ["HYPRLAND_INSTANCE_SIGNATURE"]}/.socket2.sock'
    sock.connect(server_address)

    # Main event loop
    while True:
        new_event = sock.recv(4096).decode("utf-8")
        
        for item in new_event.split("\n"):
                
            if "workspace>>" == item[0:11]:
                workspaces_num = item[-1]

                update_workspace(int(workspaces_num))
