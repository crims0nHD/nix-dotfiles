# Here lies all the config for gui programs such as wl compositors and various gui applications

{pkgs, pkgs-unstable, recursiveMerge}:
let
	eww = import ./eww.nix {inherit pkgs; inherit pkgs-unstable; };
	hypr = import ./hyprland.nix {inherit pkgs; inherit pkgs-unstable; };
	qutebrowser = import ./qutebrowser.nix {inherit pkgs; inherit pkgs-unstable; };
	wallpapers = import ./wallpapers.nix {inherit pkgs; inherit pkgs-unstable; };
	wofi = import ./wofi.nix {inherit pkgs; inherit pkgs-unstable; };
	fonts = import ./fonts.nix {inherit pkgs; inherit pkgs-unstable; };
	kitty = import ./kitty.nix {};

in
	recursiveMerge [eww hypr qutebrowser wallpapers wofi fonts kitty]
