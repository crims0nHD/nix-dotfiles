{pkgs, ...}:
{
	home = {
	file.".config/qutebrowser" = {
		source = ./qutebrowser;
		recursive = true;
	};

	packages = with pkgs; [
		qutebrowser
	];
	};
}
