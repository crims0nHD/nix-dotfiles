{pkgs, ...}:
{
	home = {
	file.".config/wofi" = {
		source = ./wofi;
		recursive = true;
	};

	packages = with pkgs; [
		wofi
	];
	};
}
