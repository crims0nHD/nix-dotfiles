{pkgs, ...}:
let
	custom-font-package = pkgs.stdenv.mkDerivation {
		name = "home-manager-fonts";
		src = ./fonts;
		installPhase = ''
			mkdir -p $out/share/fonts/truetype
			cp -r $src/* $out/share/fonts/truetype/ 
		'';
	};
in
{
	fonts.fontconfig.enable = true;
	home.packages = [ custom-font-package ];	
}
