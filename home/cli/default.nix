{...}:
{
  programs.zsh = {
	enable = true;
	};

  programs.starship = {
	enable = true;
	enableZshIntegration = true;
  };

  programs.tmux = {
	enable = true;
	clock24 = true;
	prefix = "M-x";
	secureSocket = true;
  };

}
